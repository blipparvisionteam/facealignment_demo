This repository holds demo code which calls the Face Alignment code in the helios library.
The implemented methods are based on the parper "3000 FPS via Regressing Local Binary Features"

* Make sure to clone using git's `--recursive` option! This initializes the submodules (helios, nucleus, external) and sets them to the correct commits.
* You will need to have the following libraries installed via your package manager:

        boost_filesystem
        boost_program_options
        libjpeg
        libpng
        zlib
        opencv

    In case one or more of these are not present, the CMake script should complain about it.

* Inside the facelignment_demo folder, create a build directory and run cmake as follows:

        cd facealignment_demo
        mkdir build
        cd build
        cmake ..
        make -j8

* Webcam Demo

    To demo face alignment using the webcam run `./Demo` in the `build` directory.
    The program uses the detection model `data/haarcascade_frontalface_alt.xml` and the alignment model `data/regression_model_interleaved.model`.

* Training

    To train a new face alignment model run `./Train` in the `build` directory.
    The program uses precomputed "shapes" model `data/lfpw_trainset_first100.shapes` which contains links to image-files, precomputed bounding boxes, and the ground-truth face landmarks. The bounding boxes were computed using OpenCV's standard face-detector.