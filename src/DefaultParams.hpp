# pragma once

#include <array>

namespace blippar {
namespace face {

struct params
{
	static const int numLandmarks = 68;
	static const int numTrees = 10;
	static const int treeDepth = 5 - 1;
	static const int numStages = 7;
	static const int featureDim = 4;
	using Scalar = float;
	using RFThreshold = int;
	using Weight = short;
};

}
}

