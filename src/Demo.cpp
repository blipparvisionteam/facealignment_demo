#include <cstdlib>
#include <ShapeAlignmentFromCamera.hpp>

int main(int argc, char** argv)
{
	int cam_index = 0;
	if(argc > 1)
		cam_index = std::atoi(argv[1]);

	std::string model_filename = "../data/regression_model_interleaved.model";
	std::string detection_model_filename = "../data/haarcascade_frontalface_alt.xml";

	blippar::shapeAlignmentFromCamera(model_filename, detection_model_filename, cam_index);

	return 0;
}
