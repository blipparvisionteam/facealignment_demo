# pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <nucleus/img/OpenCV.hpp>
#include <nucleus/base/Log.hpp>

#include <helios/face/FaceTracker.hpp>
#include <helios/face/FaceAlignment.hpp>
#include <helios/face/FaceParts.hpp>

namespace blippar {

	template<typename FaceMarkersT>
	void drawLandmarks(cv::Mat& image, const FaceMarkersT& landmarks, const cv::Scalar& color)
	{
		auto drawLine = [&image, &landmarks](auto landmarkIndexA, auto landmarkIndexB, auto color) {
				cv::line(image, cv::Point2d(landmarks[landmarkIndexA].x, landmarks[landmarkIndexA].y), cv::Point2d(landmarks[landmarkIndexB].x, landmarks[landmarkIndexB].y), color, 1, 8, 0); 
			};

		for(auto facePart: face::getFaceParts())
		{
			auto partColor = color;
			if(facePart.name() == face::PartName::Lefteye)
			{
				partColor = cv::Scalar(255, 0, 0);
			}
				
			for(int i = facePart.indexRange().first; i < facePart.indexRange().second - 1; i++)
			{
				drawLine(i, i + 1, partColor);
			}

			if(facePart.round())
			{
				drawLine(facePart.indexRange().second - 1,  facePart.indexRange().first, partColor);
			}
		}
	}

	void shapeAlignmentFromCamera(const std::string& model_filename, const std::string& faceDetectorModelFilename, const int camIndex)
	{
		cv::VideoCapture cap(camIndex);
		cap.set( CV_CAP_PROP_FRAME_WIDTH, 640);
		cap.set( CV_CAP_PROP_FRAME_HEIGHT, 480);
		if (!cap.isOpened())
		{
			Log() << "Not able to open camera. Exiting";
			return ;
		}

		face::FaceTracker faceTracker(faceDetectorModelFilename);
		face::FaceAlignment faceAlignment(model_filename);

		while (cv::waitKey(30) != 27)
		{
			cv::Mat coloredFrame;
			cap >> coloredFrame; // get a new frame from camera
			if (coloredFrame.empty())
			{
				Log() << "Not able to read images from the camera. Exiting";
				exit(0);
			}

			cv::Mat frame;
			cvtColor(coloredFrame, frame, CV_BGR2GRAY);
			img::ImageB imageb = img::wrap<uint8_t>(frame);

			auto identifiedBoundingBoxes = faceTracker.detectFaces(imageb);
			auto identifiedFaces = faceAlignment.alignFaces(identifiedBoundingBoxes, imageb);

			for(auto identifiedBoundingBox: identifiedBoundingBoxes)
			{
				auto boundingBox = identifiedBoundingBox.second;
				rectangle(coloredFrame, cvPoint(boundingBox.lcX ,boundingBox.lcY),
				  cvPoint(boundingBox.lcX+boundingBox.width,boundingBox.lcY + boundingBox.height),cv::Scalar(0,255,255), 1, 8, 0);
			}

			for(auto identifiedFace: identifiedFaces)
			{
				drawLandmarks(coloredFrame, identifiedFace.second, cv::Scalar(0, 255, 0));
			}

			imshow("faces", coloredFrame);
		}
	}

}

