#include <iostream>
#include <random>
#include <nucleus/base/EigenTypedefs.hpp>
#include <helios/face/Regressor.hpp>
#include <helios/face/LearnedModelIO.hpp>
#include <helios/face/LoadShapes_noopencv.hpp>
#include <DefaultParams.hpp>

int main()
{
	using namespace blippar;
	using namespace face;
	std::string trainset_filename = "../data/lfpw_trainset_first100.shapes";
	std::string model_filename = "model.serial";
	std::mt19937 rng(1);
	LogSettings::setLogLevel(LogLevel::Message);

	const double overlapRatio = 0.4;
	static const int augmentationFactor = 10;
	const std::array<int, params::numStages> numSplitPicksPerStage = {500, 500, 500, 300, 300, 300, 200};
	const std::array<double, params::numStages> maxRadiusRatios = {0.3, 0.2, 0.15, 0.12, 0.10, 0.10, 0.08};

	auto trainShapes = loadShapes(trainset_filename, params::numLandmarks);
	auto augmentedShapes = augmentShapes(trainShapes, augmentationFactor, rng);

	Regressor<params::numStages, params::numTrees, params::treeDepth, params::featureDim, params::numLandmarks, params::Scalar, params::RFThreshold, params::Weight> regressor;

	vector_Matrix<float, params::numLandmarks, 2> landmarksEstimates;
	vector_Matrix<float, params::numLandmarks, 2> landmarksGroundTruths;
	for(auto& shape: augmentedShapes)
	{
		landmarksEstimates.push_back(shape.landmarksPredicted_.cast<params::Scalar>());
		landmarksGroundTruths.push_back(shape.landmarksGroundTruth_.cast<params::Scalar>());
	}

	auto reportError = [&]{ std::cout << "error = " << computeLandmarkErrorCompleteFaceMean(landmarksGroundTruths, landmarksEstimates) << std::endl;};

	auto model = regressor.learn(augmentedShapes, landmarksEstimates, rng, maxRadiusRatios, numSplitPicksPerStage, overlapRatio, reportError);

	saveModel(model, model_filename, 1);
	return 0;
}
